Name:		python-ldap
Version:	3.4.4
Release:	1
Summary:	An object-oriented API to access LDAP directory servers
License:	Python-2.0
URL:		http://python-ldap.org/
Source0:	https://files.pythonhosted.org/packages/source/p/%{name}/%{name}-%{version}.tar.gz

BuildRequires:	gcc openldap-devel
BuildRequires:  python3-devel python3-setuptools

%description
python-ldap provides an object-oriented API for working with LDAP within\
Python programs.  It allows access to LDAP directory servers by wrapping the\
OpenLDAP 2.x libraries, and contains modules for other LDAP-related tasks\
(including processing LDIF, LDAPURLs, LDAPv3 schema, etc.).

%package -n     python3-ldap
Summary:        An object-oriented API to access LDAP directory servers

Requires:  	openldap >= 2.4.45-4 python3-pyasn1 >= 0.3.7
Requires:  	python3-pyasn1-modules >= 0.1.5 python3-setuptools
%{?python_provide:%python_provide python3-ldap}
Obsoletes: 	python3-pyldap < 3
Provides:  	python3-pyldap = %{version}-%{release}
Provides:  	python3-pyldap%{?_isa} = %{version}-%{release}

%description -n python3-ldap
python-ldap provides an object-oriented API for working with LDAP within\
Python programs.  It allows access to LDAP directory servers by wrapping the\
OpenLDAP 2.x libraries, and contains modules for other LDAP-related tasks\
(including processing LDIF, LDAPURLs, LDAPv3 schema, etc.).

%package_help

%prep
%autosetup -p1 -n %{name}-%{version}
find . -name '*.py' | xargs sed -i '1s|^#!/usr/bin/env python|#!%{__python3}|'
sed -i 's,-Werror,-Wignore,g' tox.ini
%build
%py3_build

%install
%_bindir/python3 setup.py install -O1 --skip-build --root %buildroot

%check

%files -n python3-ldap
%defattr(-,root,root)
%license LICENCE
%{_libdir}/python%{python3_version}/site-packages/*

%files help
%defattr(-,root,root)
%doc CHANGES README TODO Demo

%changelog
* Thu Feb 1 2024 liubo <liubo335@huawei.com> - 3.4.4-1
- Type: requirement
- ID: NA
- SUG: NA
- DESC: update to version 3.4.4

* Fri Aug 05 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 3.3.1-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix a system error and optimize the checking of LDAP results

* Fri Aug 05 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 3.3.1-4
- Type:requirements
- Id:NA
- SUG:NA
- DESC:modify to generate debug rpms

* Tue Jul 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 3.3.1-3
- Type:CVE
- Id:CVE-2021-46823
- SUG:NA
- DESC:fix CVE-2021-46823

* Fri Mar 04 2022 zhaoshuang <zhaoshuang@uniontech.com> - 3.3.1-2
- remove some unnecessary buildrequires

* Tue Feb 02 2021 xihaochen <xihaochen@huawei.com> - 3.3.1-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update python-ldap to 3.3.1

* Wed Oct 14 2020 shixuantong<shixuantong@huawei.com> - 3.1.0-4
- delete useless buildrequires

* Tue Jun 23 2020 wangchong <wangchong56@huawei.com> - 3.1.0-3
- replace numbers with macros

* Mon Jun 22 2020 wangchong <wangchong56@huawei.com> - 3.1.0-2
- fix the build error

* Tue Feb 11 2020 huzunhao<huzunhao2@huawei.com> - 3.1.0-1
- Package init
